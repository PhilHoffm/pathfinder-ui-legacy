# Patch Notes
## Compatibility
- Ready for v9 version.

## v9.255.1
- fix issue [#1](https://gitlab.com/sasmira/pathfinder-ui-legacy/-/issues/1)

## v9.249.5
- update support Minimal Ui (see option)

## v9.249.4
- update support Minimal Ui (see option)

## v9.249.3
- update support Minimal Ui (see option)
- fix errors console

## v9.249.2
- Add support Minimal Ui (see option)
- add support DF Chat Enhancements

## v9.249.1
- New Dicefinder with 3 templates :
- Dicefinder Basic 
- Dicefinder Campaign 
- Dicefinder Dark Mode

## v9.245.4
- Fix Combat Carousel controls icon 

## v9.245.3
- scene thumb size

## v9.245.2
- Sets the length of the navigation bar and the loading bar.

## v9.245.1
- Sidebar Macro Icon fixed

## v9.242.2
- fix Nav bar lengh

## v9.242.1
- Macro Bar can to be move now with Minimal Ui

## v9.238.3
- Sidebar Macro Icon support added thank's you @Arcanist

## v9.238.2
- Back to the olds icons !

## v9.238.1
- fix sidebar collapsed thank's you @Prolice for your help

## v9.235.1
- First release for Foundry VTT v9

## v2.4.3
- clean js

## v2.4.2
- fix a scaling error with minimal ui
- Alternative Pause Icon fix

## v2.4.0
- Support Combat Carousel like Pathfinder Ui v3
- New Dicefinder like Pathfinder Ui v3
- New Game Paused
- Fix somes minors bugs

## v2.3.7
- Initial Release for Pathfinder Legacy v2